'use strict';

var isFirstLoad = false;
var isSecondLoad = false;
var numToLoad = 10;
var currentlyLoaded = [];
var dataBucket;

//Function to get ajax
function getData(url) {
	return new Promise( function(resolve) {
		$.getJSON(url, function(data){
			resolve(data);
		});
	});
}

//Function to append 
function appendData(data){
	var sort = localStorage.getItem("sortby");

	if (sort) {
		data = _.map(_.sortByAll(data, sort));
	}

	var dataObj = {dataset: data};
	var source   = $('#entry-template').html();
	var template = Handlebars.compile(source);
	var html    = template(dataObj);

	$('.entry').remove()
	$('#content').append(html);
}

//
function loadSome(){
	var loadMore = [];
	var count;

	if (dataBucket.length > numToLoad) {
		count = numToLoad;
	} else {
		count = dataBucket.length;
		isSecondLoad = false
	}

	for(var i = 0; i < count; i++) {
		var dat = dataBucket.shift();
		loadMore.push(dat);
		currentlyLoaded.push(dat)
	}

	appendData(currentlyLoaded);
}

function clickSort ( column ) {
	var selector = column === 'words' ? '#wordSort' : '#submitSort'
	$(selector).on('click', function(){
		var sort = _.map(_.sortByAll(currentlyLoaded, column));
		appendData(sort);
	});

	localStorage.setItem('sortby', column);

}

getData('data/articles.json').then( function(data){
	dataBucket = data;
	isFirstLoad = true;
	loadSome();
});

$('#loadMore').on('click', function() {
	if (isFirstLoad || isSecondLoad) {
		loadSome();

		if (!isSecondLoad && !isFirstLoad) {
			$('#loadMore').attr( 'disabled', 'disabled' );
			isSecondLoad = false;
		}

		if (dataBucket.length === 0 && isFirstLoad) {
			isFirstLoad = false;
		}

	} else {
		getData('data/more-articles.json').then( function(data){
			dataBucket = data;
			loadSome();
		});
		isSecondLoad = true;
	}
});

clickSort('words');
clickSort('submitted');
